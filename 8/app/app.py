import os
from flask.helpers import make_response
from flask.json import jsonify
from flask import request
from flask import Flask
app = Flask(__name__)

# This is dummy orders persistent :)
orders = []

@app.route("/orders", methods=['POST'])
def add_order():
    params = request.get_json()
    order = {
        'number': len(orders) + 1,
        'name': params['name']
    }
    orders.append(order)
    response = make_response(jsonify({
        'order': order
    }), 201)
    response.headers['Location'] = os.path.join(request.url, str(order['number']))
    return response

@app.route('/orders', methods=['GET'])
def orders_list():
    return make_response(jsonify({
        'orders': orders
    }))

@app.route('/orders/<number>', methods=['GET'])
def get_order(number):
    number = int(number) - 1
    if number >= len(orders):
        return make_response('', 404)

    return make_response(jsonify({
        'order': orders[number]
    }))

@app.route('/orders/<number>', methods=['OPTION'])
def option_order(number):
    response = make_response('', 200)
    response.headers['Allow'] = 'GET, PUT'
    return response

@app.route('/orders/<number>', methods=['PUT'])
def update_order(number):
    if request.headers.get('Expect') == '100-Continue':
        return make_response('', 100)

    number = int(number) - 1
    if number >= len(orders):
        return make_response('', 404)

    params = request.get_json()
    order = orders[number]
    for (k, v) in params.items():
        order[k] = v

    response = make_response(jsonify({
          'order': orders[number]
    }))
    response.headers['Location'] = os.path.join(request.url, str(order['number']))
    return response


if __name__ == "__main__":
    app.run(debug=True)
