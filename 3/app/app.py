from flask.templating import render_template
from flask import Flask
from models.news import News
app = Flask(__name__)

@app.route('/')
def index():
    pass

@app.route('/news')
def news():
    news_list = News.find()
    return render_template('news.html', news=news_list)

@app.route('/news/<int:news_id>')
def news_item(news_id):
    news = News.find_one(news_id)
    return render_template('single_news.html', news=news)

if __name__ == '__main__':
    app.run(debug=True)