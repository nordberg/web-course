class News(object):
    idn = None
    title = ''
    description = ''

    def __init__(self, idn, title, description):
        self.idn = idn
        self.title = title
        self.description = description

    @staticmethod
    def find():
        return fixtures

    @staticmethod
    def find_one(idn):
        return next((x for x in fixtures if x.idn == idn), None)

fixtures = [
    News(1, 'Awesome news 1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, repudiandae odit pariatur tempore sed laborum molestiae iusto quaerat asperiores iure. Quod, voluptas quos aliquid repudiandae quibusdam laudantium modi quisquam esse!'),
    News(2, 'Awesome news 2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, repudiandae odit pariatur tempore sed laborum molestiae iusto quaerat asperiores iure. Quod, voluptas quos aliquid repudiandae quibusdam laudantium modi quisquam esse!'),
    News(3, 'Awesome news 3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda, repudiandae odit pariatur tempore sed laborum molestiae iusto quaerat asperiores iure. Quod, voluptas quos aliquid repudiandae quibusdam laudantium modi quisquam esse!'),
]
